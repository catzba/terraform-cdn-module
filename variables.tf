#-----------------------------------------------------------------------------------------------------
# GLOBAL VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "aws_region" {
  type = string
}

#-----------------------------------------------------------------------------------------------------
# VARIABLES
#-----------------------------------------------------------------------------------------------------

variable "domain" {
  type = any
}

variable "aliases" {
  type = any
}

variable "comment" {
  type = any
}

variable "is_ipv6_enabled" {
  type = any
}

variable "default_root_object" {
  type = any
}

variable "enabled" {
  type = any
}

variable "price_class" {
  type = any
}

variable "custom_error_response" {
  type = any
}

variable "allowed_methods" {
  type = any
}

variable "cached_methods" {
  type = any
}

variable "query_string" {
  type = any
}

variable "forward" {
  type = any
}

variable "viewer_protocol_policy" {
  type = any
}

variable "min_ttl" {
  type = any
}

variable "default_ttl" {
  type = any
}

variable "max_ttl" {
  type = any
}

variable "restriction_type" {
  type = any
}

variable "cloudfront_default_certificate" {
  type = any
}

variable "acm_certificate_arn" {
  type = any
}

variable "ssl_support_method" {
  type = any
}

variable "iam_atachment_username" {
  type    = string
  default = ""
}

variable "iam_atachment_groupname" {
  type    = string
  default = ""
}
