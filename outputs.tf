output "target_dns_name" {
  description = "Id of backend-sg"
  value       = aws_cloudfront_distribution.cloudfront_distribution.domain_name
}

output "target_zone_id" {
  description = "Id of backend-sg"
  value       = aws_cloudfront_distribution.cloudfront_distribution.hosted_zone_id
}