#-----------------------------------------------------------------------------------------------------
# S3 and CloudFront
#-----------------------------------------------------------------------------------------------------

resource "aws_s3_bucket" "cloudfront" {
  bucket = var.domain
  acl    = "private"

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST"]
    allowed_origins = ["*"]
    expose_headers  = ["ETag"]
    max_age_seconds = 3000
  }

  policy = <<EOF
{
    "Version": "2008-10-17",
    "Id": "PolicyForCloudFrontPrivateContent",
    "Statement": [
        {
            "Sid": "1",
            "Effect": "Allow",
            "Principal": {
                "AWS": "${aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn}"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::${var.domain}/*"
        }
    ]
}
EOF
}
#REQUIRED: Policy for user that allows list all buckets
resource "aws_iam_policy" "listener-policy" {
  name        = "S3-CDN-${var.domain}-Listener"
  path        = "/"
  # description = "My test policy"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:ListAllMyBuckets",
                "s3:ListBucket",
                "s3:HeadBucket"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

#REQUIRED: Policy for user that allows it to upload
resource "aws_iam_policy" "writer-policy" {
  name        = "S3-CDN-${var.domain}-Writer"
  path        = "/"
  # description = "My test policy"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
    {
        "Effect": "Allow",
        "Action": "s3:*",
        "Resource": [
            "${aws_s3_bucket.cloudfront.arn}",
            "${aws_s3_bucket.cloudfront.arn}/*"
        ]
     }]
}
EOF
}

locals {
  aws_iam_policy_attachment = var.iam_atachment_username != "" || var.iam_atachment_groupname != "" ? 1 : 0
}


#OPTIONAL: Attach listener policy to group or user
resource "aws_iam_policy_attachment" "listener-attach" {
  count = local.aws_iam_policy_attachment
  name       = "listener-attach-${var.domain}"
  users      = [var.iam_atachment_username]
  groups     = [var.iam_atachment_groupname]
  policy_arn = aws_iam_policy.listener-policy.arn
}

#OPTIONAL: Attach writer policy to group or user
resource "aws_iam_policy_attachment" "writer-attach" {
  count = local.aws_iam_policy_attachment
  name       = "writer-attach-${var.domain}"
  users      = [var.iam_atachment_username]
  groups     = [var.iam_atachment_groupname]
  policy_arn = aws_iam_policy.writer-policy.arn
}


#-----------------------------------------------------------------------------------------------------
# CLOUD FRONT DISTRIBUTION
#-----------------------------------------------------------------------------------------------------
#REQUIRED: Create Cloudfront origin access identity
resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  comment = "access-identity-${var.domain}.s3.amazonaws.com"
}

#REQUIRED: Create Cloudfront distribution
resource "aws_cloudfront_distribution" "cloudfront_distribution" {
  # depends_on = [data.aws_acm_certificate.cloudfront]

  aliases             = var.aliases
  comment             = var.comment
  is_ipv6_enabled     = var.is_ipv6_enabled
  default_root_object = var.default_root_object
  enabled             = var.enabled
  price_class         = var.price_class

  origin {
    domain_name = aws_s3_bucket.cloudfront.bucket_domain_name
    origin_id   = aws_s3_bucket.cloudfront.id

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity.cloudfront_access_identity_path
    }
  }

  # If there is a 403, return index.html with a HTTP 200 Response
  dynamic "custom_error_response" {
    for_each = var.custom_error_response

    content {
      error_caching_min_ttl = custom_error_response.value.error_caching_min_ttl
      error_code            = custom_error_response.value.error_code
      response_code         = custom_error_response.value.response_code
      response_page_path    = custom_error_response.value.response_page_path
    }
  }

  default_cache_behavior {
    allowed_methods  = var.allowed_methods
    cached_methods   = var.cached_methods
    target_origin_id = aws_s3_bucket.cloudfront.bucket

    # Forward all query strings, cookies and headers
    forwarded_values {
      query_string = var.query_string

      cookies {
        forward = var.forward
      }

    }

    viewer_protocol_policy = var.viewer_protocol_policy
    min_ttl                = var.min_ttl
    default_ttl            = var.default_ttl
    max_ttl                = var.max_ttl
  }

  # Distributes content to US and Europe


  # Restricts who is able to access this content
  restrictions {
    geo_restriction {
      # type of restriction, blacklist, whitelist or none
      restriction_type = var.restriction_type
    }
  }

  # SSL certificate for the service.

  viewer_certificate {
    cloudfront_default_certificate = var.cloudfront_default_certificate
    acm_certificate_arn            = var.acm_certificate_arn
    ssl_support_method             = var.ssl_support_method
  }


}
